# Frontend Mentor - Todo app

![Design preview for the Todo app coding challenge](./design/desktop-preview.jpg)

## 🤖 Links

- Live site: https://fem-todo-app-drab.vercel.app/
- Solution page: <https://www.frontendmentor.io/solutions/todo-app-w-react-redux-styledcomponents-reactbeautifuldnd-LTGOFaIDL0>

## 🐸 Notes

Used `redux` and `styled-components` for the first time. Also `react-beautiful-dnd` and `redux-persist`. That was not easy for me, not gonna lie 😺

Really proud of the way I [implemented](https://gitlab.com/turtlecrab/fem-todo-app/-/blob/master/src/store/todoSlice.ts#L79) reordering with filters, it keeps the pattern of finished and active todos which seems to me as the most natural way of how it should behave.

## ✨ Stuff I learned

### Adding gradient to a border:

```css
background: linear-gradient(#fff, #fff) padding-box,
            linear-gradient(magenta, cyan) border-box;
border: 1px solid transparent;
```

### Steps to set up `react-beautiful-dnd`

- Wrap the app with `<DragDropContext>` with the required `onDragEnd` callback prop.

  ```jsx
  function onDragEnd(result) {} // Synchronous state update

  return <DragDropContext onDragEnd={onDragEnd}>...</DragDropContext>
  ```

- Wrap droppable area with `<Droppable>` with the required `droppableId` prop.
  - children as a render prop
  - `innerRef`
  - `...provided.droppabaleProps`
  - `provided.placeholder`
  ```jsx
  return (
    <Droppable droppableId={id}>
      {provided => (
        <List ref={provided.innerRef} {...provided.droppableProps}>
          {list.map(item => <ListItem ... />)}
          {provided.placeholder}
        </List>
      )}
    </Droppable>
  )
  ```
- Wrap draggable item with `<Draggable>`, 2 required props: `draggableId`, `index`
  - children = render prop
  - ref=`provided.innerRef`
  - `...provided.droppableProps`
  - `...provided.dragHandleProps`
  ```jsx
  return (
    <Draggable draggableId={id} index={index}>
      {provided => (
        <Item
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        />
      )}
    </Draggable>
  )
  ```

### Adding `redux-persist` to `redux`

full `store/index.ts`:
```ts
import { configureStore, combineReducers } from '@reduxjs/toolkit'
import {
  persistStore,   // to return persistor
  persistReducer, // to wrap our manually combined reducer
  FLUSH,
  REHYDRATE,
  PAUSE,          // to add these actions to ignore list
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist'
import storage from 'redux-persist/lib/storage' // localStorage config

import todoSlice from './todoSlice'

const persistConfig = { // full config
  key: 'root',
  storage,
}

const rootReducer = combineReducers({ // so we can wrap it
  todos: todoSlice,
})

const persistedReducer = persistReducer(persistConfig, rootReducer) // ok

const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware => {  // ok just copypaste this crap
    return getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    })
  },
})

export default store
export const persistor = persistStore(store) // used in <PersistGate>
```
`index.tsx`:
```jsx
import { PersistGate } from 'redux-persist/integration/react'
import store, { persistor } from './store'

...
root.render(
  // loading can be null or any component
  <PersistGate persistor={persistor} loading={null}>
    <Provider store={store}>
      <App />
    </Provider>
  </PersistGate>
)
```