import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface ITodo {
  id: string
  text: string
  finished: boolean
}

export type ITodoFilter = 'all' | 'active' | 'completed'

export interface ITodoState {
  todos: ITodo[]
  filter: ITodoFilter
}

const initialState: ITodoState = {
  todos: [
    {
      id: '1',
      text: 'Complete online JavaScript course',
      finished: true,
    },
    {
      id: '2',
      text: 'Jog around the park 3x',
      finished: false,
    },
    {
      id: '3',
      text: '10 minutes meditation',
      finished: false,
    },
    {
      id: '4',
      text: 'Read for 1 hour',
      finished: false,
    },
    {
      id: '5',
      text: 'Pick up groceries',
      finished: false,
    },
    {
      id: '6',
      text: 'Complete Todo App on Frontend Mentor',
      finished: false,
    },
  ],
  filter: 'all',
}

const todoSlice = createSlice({
  name: 'todo',
  initialState,
  reducers: {
    addTodo(state, action: PayloadAction<{ id: string; text: string }>) {
      const { id, text } = action.payload
      state.todos.unshift({
        id,
        text,
        finished: false,
      })
    },
    toggleTodo(state, action) {
      const id = action.payload
      const todo = state.todos.find(todo => todo.id === id)
      if (todo) todo.finished = !todo.finished
    },
    deleteTodo(state, action) {
      const id = action.payload
      state.todos = state.todos.filter(todo => todo.id !== id)
    },
    clearCompleted(state) {
      state.todos = state.todos.filter(todo => !todo.finished)
    },
    filterTodos(state, action: PayloadAction<ITodoFilter>) {
      state.filter = action.payload
    },
    reorderTodo(
      state,
      action: PayloadAction<{ from: number; to: number; filter: ITodoFilter }>
    ) {
      const { from, to, filter } = action.payload

      if (filter === 'all') {
        const [swap] = state.todos.splice(from, 1)
        state.todos.splice(to, 0, swap)
        return
      }

      const stencil = state.todos.map(todo => todo.finished)
      const active = state.todos.filter(todo => !todo.finished)
      const completed = state.todos.filter(todo => todo.finished)

      if (filter === 'active') {
        const [swap] = active.splice(from, 1)
        active.splice(to, 0, swap)
      } else {
        const [swap] = completed.splice(from, 1)
        completed.splice(to, 0, swap)
      }

      state.todos = stencil.map(value =>
        value ? completed.shift()! : active.shift()!
      )
    },
  },
})

export default todoSlice.reducer
export const {
  addTodo,
  toggleTodo,
  deleteTodo,
  clearCompleted,
  filterTodos,
  reorderTodo,
} = todoSlice.actions
