import styled from 'styled-components'
import { useDispatch, useSelector } from 'react-redux'
import {
  DragDropContext,
  DropResult,
  ResponderProvided,
} from 'react-beautiful-dnd'

import Header from './components/Header'
import Todos from './components/Todos'
import Attribution from './components/Attribution'
import { ITodoState, reorderTodo } from './store/todoSlice'

function App() {
  const filter = useSelector(
    (state: { todos: ITodoState }) => state.todos.filter
  )
  const dispatch = useDispatch()

  function onDragEnd(result: DropResult, provided: ResponderProvided) {
    if (!result.destination) return

    const from = result.source.index
    const to = result.destination.index

    dispatch(reorderTodo({ from, to, filter }))
  }

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <AppContainer>
        <Header />
        <Main>
          <Todos />
        </Main>
        <footer>
          <Attribution />
        </footer>
      </AppContainer>
    </DragDropContext>
  )
}

const AppContainer = styled.div`
  padding: 48px 24px 12px;
  min-height: 100vh;
  background-image: var(--bg-mobile-url);
  background-repeat: no-repeat;
  background-size: auto;
  background-position: top center;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: var(--body-bg);

  @media screen and (min-width: 376px) {
    background-image: var(--bg-desktop-url);
  }

  @media screen and (min-width: 600px) {
    padding-top: 77px;
  }

  @media screen and (min-width: 1441px) {
    background-size: contain;
  }
`

const Main = styled.main`
  flex: 1;
  width: 100%;
  max-width: 540px;
`

export default App
