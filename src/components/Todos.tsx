import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import { Droppable } from 'react-beautiful-dnd'

import Todo from './Todo'
import TodoInput from './TodoInput'
import FilterButton from './FilterButton'

import { ITodoState, clearCompleted } from '../store/todoSlice'

function Todos() {
  const { todos, filter } = useSelector(
    (state: { todos: ITodoState }) => state.todos
  )
  const dispatch = useDispatch()

  const itemsLeft = todos.filter(todo => !todo.finished).length
  const filteredTodos =
    filter === 'all'
      ? todos
      : filter === 'completed'
      ? todos.filter(todo => todo.finished)
      : todos.filter(todo => !todo.finished)

  const noTodosToShow = filteredTodos.length === 0

  return (
    <Container>
      <TodoInput />
      <Droppable droppableId="todo-list">
        {provided => (
          <TodoList ref={provided.innerRef} {...provided.droppableProps}>
            {filteredTodos.map((todo, i) => (
              <Todo
                text={todo.text}
                id={todo.id}
                finished={todo.finished}
                key={todo.id}
                index={i}
              />
            ))}
            {provided.placeholder}
          </TodoList>
        )}
      </Droppable>
      {todos.length ? (
        <>
          <TodosFooter roundAllCorners={noTodosToShow}>
            <div>
              {itemsLeft} item{itemsLeft === 1 ? '' : 's'} left
            </div>
            <FilterLinksDiv>
              <FilterButton filter="all" activeFilter={filter} />
              <FilterButton filter="active" activeFilter={filter} />
              <FilterButton filter="completed" activeFilter={filter} />
            </FilterLinksDiv>
            <div>
              <ClearCompletedButton onClick={() => dispatch(clearCompleted())}>
                Clear Completed
              </ClearCompletedButton>
            </div>
          </TodosFooter>
          <InfoBottomText>Drag and drop to reorder list</InfoBottomText>
        </>
      ) : null}
    </Container>
  )
}

const Container = styled.div`
  margin: 33px 0 43px;

  @media screen and (min-width: 600px) {
    margin: 40px 0 50px;
  }
`

const TodoList = styled.ul`
  margin: 16px 0 0;
  padding: 0;
  list-style: none;
  box-shadow: var(--shadow);

  @media screen and (min-width: 600px) {
    margin-top: 24px;
  }
`

const TodosFooter = styled.div<{ roundAllCorners?: boolean }>`
  position: relative;
  padding: 0 20px;
  min-height: 50px;
  background-color: var(--todos-bg);
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-radius: ${props => (props.roundAllCorners ? '5px' : '0 0 5px 5px')};
  box-shadow: var(--shadow);

  font-family: var(--ff);
  font-weight: 400;
  font-size: 12px;
  letter-spacing: -0.015em;
  color: var(--bottom-text-color);

  > * {
    flex: 1;
  }

  @media screen and (min-width: 600px) {
    padding: 0 24px;
    font-size: 14px;
  }
`

const ClearCompletedButton = styled.button`
  cursor: pointer;
  padding: 0;
  background-color: transparent;
  border: none;
  font-family: var(--ff);
  font-weight: 400;
  font-size: 12px;
  letter-spacing: -0.015em;
  color: var(--bottom-text-color);
  display: block;
  margin-left: auto;

  &:hover {
    color: var(--bottom-text-color-hover);
  }

  @media screen and (min-width: 600px) {
    font-size: 14px;
  }
`

const FilterLinksDiv = styled.div`
  position: absolute;
  width: 100%;
  min-height: 48px;
  bottom: -64px;
  left: 0;
  background-color: var(--todos-bg);
  border-radius: 5px;
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: var(--shadow);

  @media screen and (min-width: 600px) {
    position: static;
    width: auto;
    box-shadow: none;
  }
`

const InfoBottomText = styled.div`
  margin-top: 107px;
  font-family: var(--ff);
  font-weight: 400;
  font-size: 14px;
  letter-spacing: -0.015em;
  text-align: center;
  color: var(--bottom-text-color);

  @media screen and (min-width: 600px) {
    margin-top: 52px;
  }
`

export default Todos
