import styled from 'styled-components'

function Attribution() {
  return (
    <Container>
      Challenge by{' '}
      <a
        href="https://www.frontendmentor.io?ref=challenge"
        target="_blank"
        rel="noreferrer"
      >
        Frontend Mentor
      </a>
      . Coded by{' '}
      <a
        href="https://www.frontendmentor.io/profile/turtlecrab"
        target="_blank"
        rel="noreferrer"
      >
        Turtle Crab
      </a>
      .
    </Container>
  )
}

const Container = styled.div`
  font-size: 11px;
  text-align: center;
  color: var(--bottom-text-color);
  font-family: var(--ff);

  a {
    color: var(--placeholder-color);
    text-decoration: none;
    
    &:hover {
      color: var(--bottom-text-color-hover);
    }
  }
`

export default Attribution
