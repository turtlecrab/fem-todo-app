import { useDispatch } from 'react-redux'
import styled from 'styled-components'

import { filterTodos, ITodoFilter } from '../store/todoSlice'

interface Props {
  filter: ITodoFilter
  activeFilter: ITodoFilter
}

function FilterButton({ filter, activeFilter }: Props) {
  const dispatch = useDispatch()

  const active = filter === activeFilter

  return (
    <Button
      className={active ? 'active' : ''}
      onClick={() => dispatch(filterTodos(filter))}
      aria-label={`Filter ${filter} todos`}
    >
      {capitalize(filter)}
    </Button>
  )
}

function capitalize(str: string) {
  return str[0].toUpperCase() + str.slice(1)
}

const Button = styled.button`
  border: none;
  background-color: transparent;
  padding: 0 9px;
  font-family: var(--ff);
  font-weight: 700;
  font-size: 15px;
  letter-spacing: -0.04em;
  color: var(--bottom-text-color);
  cursor: pointer;

  &:hover {
    color: var(--bottom-text-color-hover);
  }

  &.active {
    cursor: default;
    color: var(--bright-blue);

    &:hover {
      color: var(--bright-blue);
    }
  }
`

export default FilterButton
