import { useEffect, useState } from 'react'
import styled from 'styled-components'

import { sunIcon } from '../images/icons'
import { moonIcon } from '../images/icons'

function Header() {
  const [theme, setTheme] = useState(
    () => localStorage.getItem('theme') || 'dark'
  )

  useEffect(() => {
    if (theme === 'dark') {
      document.body.className = ''
    } else {
      document.body.classList.add('theme-light')
    }
    localStorage.setItem('theme', theme)
  }, [theme])

  return (
    <Container>
      <h1>Todo</h1>
      <button
        aria-label="Toggle visual theme"
        onClick={() => setTheme(prev => (prev === 'dark' ? 'light' : 'dark'))}
      >
        <img src={theme === 'dark' ? sunIcon : moonIcon} alt="" />
      </button>
    </Container>
  )
}

const Container = styled.header`
  display: flex;
  justify-content: space-between;
  width: 100%;
  max-width: 540px;

  h1 {
    margin: 0;
    padding: 0;
    text-transform: uppercase;
    font-family: var(--ff);
    font-weight: 700;
    font-size: 27px;
    letter-spacing: 0.36em;

    @media screen and (min-width: 600px) {
      font-size: 40px;
      letter-spacing: 0.37em;
    }
  }

  button {
    margin: 0;
    padding: 0;
    border: none;
    background-color: transparent;
    width: 20px;
    height: 20px;
    cursor: pointer;

    @media screen and (min-width: 600px) {
      margin-top: 4px;
      width: 26px;
      height: 26px;
    }

    img {
      width: 100%;
    }
  }
`

export default Header
