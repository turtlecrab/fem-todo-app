import { nanoid } from '@reduxjs/toolkit'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import styled from 'styled-components'

import { addTodo } from '../store/todoSlice'

function TodoInput() {
  const [text, setText] = useState('')
  const dispatch = useDispatch()

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault()

    const text = e.currentTarget.text.value.trim()
    if (!text) return

    const id = nanoid()
    dispatch(addTodo({ id, text }))
    setText('')
  }

  return (
    <FormContainer>
      <Form autoComplete="off" onSubmit={handleSubmit}>
        <Input
          type="text"
          name="text"
          placeholder="Create a new todo..."
          aria-label="Create a new todo"
          value={text}
          onChange={e => setText(e.currentTarget.value)}
        />
      </Form>
    </FormContainer>
  )
}

const FormContainer = styled.div`
  box-shadow: var(--shadow);
`

const Form = styled.form`
  position: relative;

  &::after {
    content: '';
    pointer-events: none;
    display: block;
    position: absolute;
    border-radius: 50%;
    border: 1px solid var(--hr-color);
    left: 20px;
    top: 14px;
    width: 20px;
    height: 20px;

    @media screen and (min-width: 600px) {
      left: 24px;
      top: 20px;
      width: 24px;
      height: 24px;
    }
  }
`

const Input = styled.input`
  padding-left: 52px;
  padding-top: 4px;
  width: 100%;
  min-height: 48px;
  background-color: var(--todos-bg);
  border: none;
  border-radius: 5px;
  font-family: var(--ff);
  font-weight: 400;
  font-size: 12px;
  letter-spacing: -0.015em;
  color: var(--main-text-color);

  &::placeholder {
    color: var(--placeholder-color);
  }

  @media screen and (min-width: 600px) {
    padding-left: 72px;
    min-height: 64px;
    font-size: 19px;
    letter-spacing: -0.04em;
  }
`
export default TodoInput
