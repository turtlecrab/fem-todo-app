import { useDispatch } from 'react-redux'
import styled from 'styled-components'
import { Draggable } from 'react-beautiful-dnd'

import { toggleTodo, deleteTodo } from '../store/todoSlice'

import { crossIcon } from '../images/icons'
import { checkIcon } from '../images/icons'

interface Props {
  id: string
  text: string
  finished: boolean
  index: number
}

function Todo({ id, text, finished, index }: Props) {
  const dispatch = useDispatch()

  return (
    <Draggable draggableId={id} index={index}>
      {provided => (
        <Container ref={provided.innerRef} {...provided.draggableProps}>
          <CheckboxContainer checked={finished}>
            <Checkbox
              type="checkbox"
              checked={finished}
              aria-labelledby={id}
              onChange={() => dispatch(toggleTodo(id))}
            />
            <div className="focus-indicator"></div>
          </CheckboxContainer>
          <Text
            id={id}
            className={finished ? 'finished' : ''}
            {...provided.dragHandleProps}
          >
            {text}
          </Text>
          <CloseButton
            aria-label="Delete todo"
            onClick={() => dispatch(deleteTodo(id))}
          >
            <img src={crossIcon} alt="" />
          </CloseButton>
        </Container>
      )}
    </Draggable>
  )
}

const Container = styled.li`
  padding: 0 20px;
  min-height: 53px;
  border-bottom: 1px solid var(--hr-color);
  background-color: var(--todos-bg);
  display: flex;
  align-items: center;

  &:first-child {
    border-radius: 5px 5px 0 0;
  }

  @media screen and (min-width: 600px) {
    padding: 0 24px;
    min-height: 65px;

    --close-btn-opacity: 0;

    &:hover {
      --close-btn-opacity: 1;
    }
  }
`

const CheckboxContainer = styled.div<{ checked: boolean }>`
  position: relative;
  display: flex;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background-image: ${props =>
    props.checked && `url(${checkIcon}), var(--cool-gradient)`};
  background-repeat: no-repeat;
  background-position: center;
  border: ${props => (props.checked ? 'none' : '1px solid var(--hr-color)')};

  ${props =>
    !props.checked &&
    `
      &:hover {
        background: linear-gradient(var(--todos-bg), var(--todos-bg)) padding-box,
          var(--cool-gradient) border-box;
        border: 1px solid transparent;
      }
    `}

  @media screen and (min-width: 600px) {
    width: 24px;
    height: 24px;
  }

  & > input[type='checkbox'] + .focus-indicator {
    pointer-events: none;
    width: 100%;
    height: 100%;
    border-radius: 50%;
  }

  & > input[type='checkbox']:focus-visible + .focus-indicator {
    outline: var(--main-text-color) auto 2px;
    outline: -webkit-focus-ring-color auto 1px;
  }
`

const Checkbox = styled.input`
  position: absolute;
  inset: 0;
  cursor: pointer;
  width: 20px;
  height: 20px;
  margin: 0;

  opacity: 0;

  @media screen and (min-width: 600px) {
    width: 24px;
    height: 24px;
  }
`

const Text = styled.div`
  cursor: pointer;
  align-self: stretch;
  margin: 8px 12px;
  padding-top: 2px;
  flex: 1;
  font-family: 'Josefin Sans', sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 12px;
  letter-spacing: -0.015em;
  display: flex;
  overflow-x: auto;

  align-items: center;
  color: var(--main-text-color);

  &.finished {
    text-decoration: line-through;
    color: var(--finished-todo-color);
  }

  @media screen and (min-width: 600px) {
    margin: 8px 24px;
    font-size: 19px;
    line-height: 19px;
    letter-spacing: -0.04em;
  }
`

const CloseButton = styled.button`
  width: 12px;
  height: 12px;
  border: none;
  margin: 0;
  padding: 0 0 2px;
  background-color: transparent;

  > img {
    width: 100%;
    cursor: pointer;
    opacity: var(--close-btn-opacity);
  }

  @media screen and (min-width: 600px) {
    width: 18px;
    height: 18px;
  }
`

export default Todo
