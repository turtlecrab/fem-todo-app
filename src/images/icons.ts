import crossIcon from './icon-cross.svg'
import checkIcon from './icon-check.svg'
import moonIcon from './icon-moon.svg'
import sunIcon from './icon-sun.svg'

export { crossIcon, checkIcon, moonIcon, sunIcon }
